static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#cedce1", "#181b33" },
	[SchemeSel] = { "#cedce1", "#C37C83" },
	[SchemeOut] = { "#cedce1", "#9FC0D3" },
};
